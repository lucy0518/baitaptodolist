export let act = document.getElementById(`newTask`).value;

export function renderTasks(taskArr) {
  let contentHTML = "";

  for (let i = 0; i < taskArr.length; i++) {
    var task = taskArr[i];

    let taskItem = `<div class="todo__wrap">
    <div class="todo__text"><p>${task}</p></div>
    <div class="todo__icons">
    <button class="btn-style" onclick ="xoaTask('${task}')">
    <i class="fa-regular fa-trash-can"></i>
    </button>
    <button class="btn-style" onclick ="completeTask('${task}')">
    <i class="fa-regular fa-circle-check"></i>
    </button>
    </div>
    </div>
    `;

    contentHTML += taskItem + "<br>";
  }

  document.getElementById(`todo`).innerHTML = contentHTML;
}

export function renderCompleteTasks(taskArr) {
  let contentHTML = "";

  for (let i = 0; i < taskArr.length; i++) {
    var task = taskArr[i];

    let taskItem = `<div class="todo__wrap2">
      <div class="todo__text2">
      <p>${task}</p>
      </div>
      <div class="todo__icons2">
      <button class="btn-style2" onclick ="xoaTask2('${task}')">
      <i class="fa-regular fa-trash-can"></i>
      </button>
      <button class="btn-style2 btn-style2-nozoom">
      <i class="check fa-solid fa-circle-check"></i>
      </button>
      </div>
      </div>
      `;

    contentHTML += taskItem + "<br>";
  }

  document.getElementById(`completed`).innerHTML = contentHTML;
}

export function findTask(text, taskArr) {
  for (var index = 0; index < taskArr.length; index++) {
    var task = taskArr[index];

    if (task == text) {
      return index;
    }
  }

  return -1;
}
