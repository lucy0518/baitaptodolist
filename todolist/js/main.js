import {
  findTask,
  renderCompleteTasks,
  renderTasks,
} from "./controller/controller.js";

const DSTASK_LOCALSTORAGE = "DSTASK_LOCALSTORAGE";
const DSCOMPLETETASK_LOCALSTORAGE = "DSCOMPLETETASK_LOCALSTORAGE";

let taskArr = [];

let dsTaskJson = localStorage.getItem(DSTASK_LOCALSTORAGE);
if (dsTaskJson != null) {
  taskArr = JSON.parse(dsTaskJson);
  console.log("taskArr: ", taskArr);

  renderTasks(taskArr);
}

document.getElementById(`addItem`).addEventListener("click", () => {
  let getTask = document.getElementById(`newTask`).value;
  taskArr.push(getTask);
  console.log("taskArr: ", taskArr);

  let dsTaskJson = JSON.stringify(taskArr);
  localStorage.setItem(DSTASK_LOCALSTORAGE, dsTaskJson);
  renderTasks(taskArr);
  document.getElementById(`newTask`).value = "";
});

document.getElementById(`newTask`).addEventListener("keydown", (e) => {
  if (e.keyCode == 13) {
    document.getElementById("addItem").click();
  }
});

function xoaTask(task) {
  let index = findTask(task, taskArr);

  if (index != -1) {
    taskArr.splice(index, 1);

    let dsTaskJson = JSON.stringify(taskArr);

    localStorage.setItem(DSTASK_LOCALSTORAGE, dsTaskJson);
    renderTasks(taskArr);
  }
}

window.xoaTask = xoaTask;

let completeArr = [];

let dsCompleteTaskJson = localStorage.getItem(DSCOMPLETETASK_LOCALSTORAGE);
if (dsCompleteTaskJson != null) {
  completeArr = JSON.parse(dsCompleteTaskJson);

  renderCompleteTasks(completeArr);
}

function completeTask(task) {
  var index = findTask(task, taskArr);

  if (index != -1) {
    taskArr.splice(index, 1);
    taskArr = taskArr.filter((f) => f !== task);
    completeArr.push(task);
    let dsCompleteTaskJson = JSON.stringify(completeArr);
    localStorage.setItem(DSCOMPLETETASK_LOCALSTORAGE, dsCompleteTaskJson);
    renderCompleteTasks(completeArr);

    let dsTaskJson = JSON.stringify(taskArr);
    localStorage.setItem(DSTASK_LOCALSTORAGE, dsTaskJson);
    renderTasks(taskArr);
  }
}

window.completeTask = completeTask;

function xoaTask2(task) {
  var index = findTask(task, completeArr);

  if (index != -1) {
    completeArr.splice(index, 1);

    let dsCompleteTaskJson = JSON.stringify(completeArr);

    localStorage.setItem(DSCOMPLETETASK_LOCALSTORAGE, dsCompleteTaskJson);
    renderCompleteTasks(completeArr);
  }
}

window.xoaTask2 = xoaTask2;

document.getElementById(`one`).addEventListener("click", () => {
  document.getElementById(`todo`).style.display = "none";
  document.getElementById(`completed`).style.display = "block";
});

document.getElementById(`two`).addEventListener("click", () => {
  taskArr.sort();
  console.log("taskArr: ", taskArr);
  completeArr.sort();

  renderTasks(taskArr);
  renderCompleteTasks(completeArr);
});

document.getElementById(`three`).addEventListener("click", () => {
  taskArr.sort().reverse();
  console.log("taskArr: ", taskArr);
  completeArr.sort().reverse();

  renderTasks(taskArr);
  renderCompleteTasks(completeArr);
});

document.getElementById(`all`).addEventListener("click", () => {
  document.getElementById(`completed`).style.display = "none";
  document.getElementById(`todo`).style.display = "block";
});
